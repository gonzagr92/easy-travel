# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: 'gonza.gr92@gmail.com.ar', password: '12qwasdzxc', password_confirmation: '12qwasdzxc');
User.create(email: 'admin@easy-travel.com.ar', password: 'easyTest#1', password_confirmation: 'easyTest#1');


vdu = Zona.create(nombre: 'Valle de Uco', descripcion: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')
cdp = Zona.create(nombre: 'Cruz de Piedra', descripcion: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')
cdm = Zona.create(nombre: 'Ciudad de Mendoza', descripcion: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')


Bodega.create(nombre: 'Salentin', icono:'http://dev-easy-travel.herokuapp.com/images/logo_salentein.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupunato.com.ar', foto: 'http://dev-easy-travel.herokuapp.com/images/foto_salentein_tercerpubli.jpg', url: 'http://bodegassalentein.com/')
Bodega.create(nombre: 'Familia Giaquinta', icono:'http://dev-easy-travel.herokuapp.com/images/finca_familia.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupungato.com.ar')
Bodega.create(nombre: 'Altus', icono:'http://dev-easy-travel.herokuapp.com/images/logo_altu.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupungato.com.ar')
Bodega.create(nombre: 'Andeluna', icono:'http://dev-easy-travel.herokuapp.com/images/logo_andeluna.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupungato.com.ar')
Bodega.create(nombre: 'Monteviejo', icono:'http://dev-easy-travel.herokuapp.com/images/logo_monteviejo.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupungato.com.ar', foto:'http://dev-easy-travel.herokuapp.com/images/foto_monteviejo_dospubli.jpg')
Bodega.create(nombre: 'La linda', icono:'http://dev-easy-travel.herokuapp.com/images/logo_finca.png', zona: vdu, direccion: 'La Vencedora s/n – Tupungato', email: 'info@altusdetupungato.com.ar', reservas: 'reservas@altusdetupungato.com.ar')

Restaurante.create(nombre: 'Anna Bistró', direccion: 'Av. Juan B. Justo 161, Mendoza', email: 'info@annabistro.com', url: 'http://www.annabistro.com', zona:dcm)
