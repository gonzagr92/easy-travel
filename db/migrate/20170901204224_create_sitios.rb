class CreateSitios < ActiveRecord::Migration[5.1]
  def change
    create_table :sitios do |t|
      t.string :nombre
      t.string :uid
      t.string :detalle
      t.references :zona, foreign_key: true

      t.timestamps
    end

    reversible do |change|
      change.up do
        add_attachment :sitios, :foto
      end

      change.down do
        remove_attachment :sitios, :foto
      end
    end
  end
end
