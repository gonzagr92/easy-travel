class CreateZonas < ActiveRecord::Migration[5.1]
  def change
    create_table :zonas do |t|
      t.string :nombre
      t.string :descripcion
      t.string :uid, index: true
      
      t.timestamps
    end
  end
end
