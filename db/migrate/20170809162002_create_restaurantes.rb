class CreateRestaurantes < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurantes do |t|
      t.string :nombre
      t.string :direccion
      t.string :coordenadas
      t.string :email
      t.string :url

      t.timestamps
    end

    reversible do |change|
      change.up do
        add_attachment :restaurantes, :icono
        add_attachment :restaurantes, :foto
      end

      change.down do
        remove_attachment :restaurantes, :icono
        remove_attachment :restaurantes, :foto
      end
    end
  end
end
