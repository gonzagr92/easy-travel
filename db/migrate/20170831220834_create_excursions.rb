class CreateExcursions < ActiveRecord::Migration[5.1]
  def change
    create_table :excursions do |t|
      t.string :direccion
      t.string :email
      t.string :reservas
      t.string :telefono
      t.string :url
      t.references :zona, foreign_key: true

      t.timestamps
    end

    reversible do |change|
      change.up do
        add_attachment :excursions, :icono
      end

      change.down do
        remove_attachment :excursions, :icono
      end
    end
  end
end
