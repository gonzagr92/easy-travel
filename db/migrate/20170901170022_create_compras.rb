class CreateCompras < ActiveRecord::Migration[5.1]
  def change
    create_table :compras do |t|
      t.string :nombre
      t.string :direccion
      t.string :coordenadas
      t.string :email
      t.string :url
      t.string :horario
      t.references :zona, foreign_key: true

      t.timestamps
    end

    reversible do |change|
      change.up do
        add_attachment :compras, :icono
      end

      change.down do
        remove_attachment :compras, :icono
      end
    end
  end
end
