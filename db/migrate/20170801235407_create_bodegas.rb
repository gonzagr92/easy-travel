class CreateBodegas < ActiveRecord::Migration[5.1]
  def change
    create_table :bodegas do |t|
      t.references :zona, index: true, foreign_key: true
      t.string :direccion
      t.string :email
      t.string :reservas
      t.string :url

      t.timestamps
    end

    reversible do |change|
      change.up do
        add_attachment :bodegas, :icono
        add_attachment :bodegas, :foto
      end

      change.down do
        remove_attachment :bodegas, :icono
        remove_attachment :bodegas, :foto
      end
    end

  end
end
