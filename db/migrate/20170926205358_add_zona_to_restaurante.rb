class AddZonaToRestaurante < ActiveRecord::Migration[5.1]
  def change
    add_reference :restaurantes, :zona, foreign_key: true
  end
end
