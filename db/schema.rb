# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170926205358) do

  create_table "bodegas", force: :cascade do |t|
    t.integer "zona_id"
    t.string "direccion"
    t.string "email"
    t.string "reservas"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icono_file_name"
    t.string "icono_content_type"
    t.integer "icono_file_size"
    t.datetime "icono_updated_at"
    t.string "foto_file_name"
    t.string "foto_content_type"
    t.integer "foto_file_size"
    t.datetime "foto_updated_at"
    t.string "nombre"
    t.index ["zona_id"], name: "index_bodegas_on_zona_id"
  end

  create_table "compras", force: :cascade do |t|
    t.string "nombre"
    t.string "direccion"
    t.string "coordenadas"
    t.string "email"
    t.string "url"
    t.string "horario"
    t.integer "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icono_file_name"
    t.string "icono_content_type"
    t.integer "icono_file_size"
    t.datetime "icono_updated_at"
    t.index ["zona_id"], name: "index_compras_on_zona_id"
  end

  create_table "excursions", force: :cascade do |t|
    t.string "direccion"
    t.string "email"
    t.string "reservas"
    t.string "telefono"
    t.string "url"
    t.integer "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icono_file_name"
    t.string "icono_content_type"
    t.integer "icono_file_size"
    t.datetime "icono_updated_at"
    t.index ["zona_id"], name: "index_excursions_on_zona_id"
  end

  create_table "logins", force: :cascade do |t|
    t.string "ip"
    t.string "ua"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "restaurantes", force: :cascade do |t|
    t.string "nombre"
    t.string "direccion"
    t.string "coordenadas"
    t.string "email"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icono_file_name"
    t.string "icono_content_type"
    t.integer "icono_file_size"
    t.datetime "icono_updated_at"
    t.string "foto_file_name"
    t.string "foto_content_type"
    t.integer "foto_file_size"
    t.datetime "foto_updated_at"
    t.integer "zona_id"
    t.index ["zona_id"], name: "index_restaurantes_on_zona_id"
  end

  create_table "sitios", force: :cascade do |t|
    t.string "nombre"
    t.string "uid"
    t.string "detalle"
    t.integer "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "foto_file_name"
    t.string "foto_content_type"
    t.integer "foto_file_size"
    t.datetime "foto_updated_at"
    t.index ["zona_id"], name: "index_sitios_on_zona_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "zonas", force: :cascade do |t|
    t.string "nombre"
    t.string "descripcion"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_zonas_on_uid"
  end

end
