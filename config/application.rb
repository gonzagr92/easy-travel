require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module EasyTravel
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.middleware.insert_before(Rack::Runtime, Rack::Rewrite) do
      r301 %r{/pesa_equipaje}, 'http://dev-easy-travel.herokuapp.com$&', :if => Proc.new { |rack_env|
        rack_env['HTTP_X_FORWARDED_PROTO'] == 'https'
      }
    end
  end
end
