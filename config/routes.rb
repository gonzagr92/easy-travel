Rails.application.routes.draw do

  scope 'admin' do
    resources :bodegas
    resources :zonas
    resources :restaurantes
    resources :excursions
    resources :compras
    resources :sitios
    get 'logs', to: 'admin#logs'
    get 'puestos', to: 'admin#puestos'
    post 'puestos', to: 'admin#puestos'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'asistencia',           to: 'index#asistencia'
  get 'bodegas',              to: 'index#bodegas',          as: :bodegas_show
  get 'checkin',              to: 'index#checkin'
  get 'checkin/:aerolinea',   to: 'index#checkin_detail'
  get 'services',             to: 'index#services'
  get 'restaurantes',         to: 'index#restaurantes',     as: :restaurantes_show
  get 'compras',              to: 'index#compras',          as: :compras_show
  get 'pesa_equipaje',        to: 'index#pesa_equipaje'
  get 'tour_services',        to: 'index#tour_services'
  get 'tour_info',            to: 'index#tour_info'
  get 'excursiones',          to: 'index#excursiones',      as: :excursiones_show
  get 'sitios',               to: 'index#sitios',           as: :sitios_show
  get 'sitio/:uid',           to: 'index#sitio_single',    as: :sitio_single

  get 'admin', to: 'admin#index'

  devise_for :users, controllers: {sessions: "sessions"}

  get ':form/:uid', to: 'index#lugares'

  root to: 'index#index'
end
