require 'test_helper'

class ExcursionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @excursion = excursions(:one)
  end

  test "should get index" do
    get excursions_url
    assert_response :success
  end

  test "should get new" do
    get new_excursion_url
    assert_response :success
  end

  test "should create excursion" do
    assert_difference('Excursion.count') do
      post excursions_url, params: { excursion: { direccion: @excursion.direccion, email: @excursion.email, reservas: @excursion.reservas, telefono: @excursion.telefono, url: @excursion.url, zona_id: @excursion.zona_id } }
    end

    assert_redirected_to excursion_url(Excursion.last)
  end

  test "should show excursion" do
    get excursion_url(@excursion)
    assert_response :success
  end

  test "should get edit" do
    get edit_excursion_url(@excursion)
    assert_response :success
  end

  test "should update excursion" do
    patch excursion_url(@excursion), params: { excursion: { direccion: @excursion.direccion, email: @excursion.email, reservas: @excursion.reservas, telefono: @excursion.telefono, url: @excursion.url, zona_id: @excursion.zona_id } }
    assert_redirected_to excursion_url(@excursion)
  end

  test "should destroy excursion" do
    assert_difference('Excursion.count', -1) do
      delete excursion_url(@excursion)
    end

    assert_redirected_to excursions_url
  end
end
