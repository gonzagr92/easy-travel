class Zona < ApplicationRecord
  has_many :bodegas
  has_many :excursions
  has_many :compras
  has_many :sitios
  has_many :restaurantes
  before_save { |t| t.uid = t.nombre.downcase.dasherize.tr(' ','-') }
end
