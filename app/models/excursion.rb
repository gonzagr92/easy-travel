class Excursion < ApplicationRecord
  belongs_to :zona

  has_attached_file :icono, styles: { original: "500", medium: "300"}
  validates_attachment_content_type :icono, content_type: /\Aimage\/.*\Z/

end
