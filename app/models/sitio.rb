class Sitio < ApplicationRecord
  belongs_to :zona

  before_save { |t| t.uid = t.nombre.downcase.dasherize.tr(' ','-') }

  has_attached_file :foto, styles: { original: "500", medium: "300"}
  validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/
end
