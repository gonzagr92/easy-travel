class Bodega < ApplicationRecord
  belongs_to :zona

  has_attached_file :icono, styles: { original: "500", medium: "300"}
  validates_attachment_content_type :icono, content_type: /\Aimage\/.*\Z/

  has_attached_file :foto, styles: { original: "500", medium: "300"}
  validates_attachment_content_type :foto, content_type: /\Aimage\/.*\Z/
end
