json.extract! excursion, :id, :direccion, :email, :reservas, :telefono, :url, :zona_id, :created_at, :updated_at
json.url excursion_url(excursion, format: :json)
