json.extract! bodega, :id, :zona, :direccion, :email, :reservas, :url, :created_at, :updated_at
json.url bodega_url(bodega, format: :json)
