json.extract! compra, :id, :nombre, :direccion, :coordenadas, :email, :url, :horario, :zona_id, :created_at, :updated_at
json.url compra_url(compra, format: :json)
