/**
 * check if element has the attribute
 * @param {text} attribute
 * @returns {Boolean}
 */
$.fn.hasAttr = function(attribute){
    var attr=false, undef;
    
    attr = $(this).attr(attribute);

    if(attr === undef || attr === false){
        return false;
    };
    if(attr === attribute){
        return true;
    };
};

//serialize form data into 
$.fn.serializeJSON = function(){
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

// $(function() {
//   $('a[href*=#]:not([href=#])').click(function() {
//     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
//       var target = $(this.hash);
//       target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
//       if (target.length) {
//         $('html,body').animate({
//           scrollTop: target.offset().top - 85
//         }, 1000);
//         return false;
//       }
//     }
//   });
// });

/**
 *jQuery Image hover
 *
 *Use img attribute img-hover-src="{{path}}" to add the path to the hover image
 *when mouse over it changes the src value to img-hover-src value
 *
 *<img src="example/img.jpg" img-hover-src="example/img_hover.jpg" />
 */
$(function() {
  $('img[img-hover-src]').mouseover(function(){
    img = $(this).attr('src');
    imgHover = $(this).attr('img-hover-src');
    $(this).attr('src', imgHover);
  }).mouseout(function(){
    $(this).attr('src', img);
  });
});