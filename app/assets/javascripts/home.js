$(document).on('turbolinks:load', function(){
	var $select = $('select.onChangeRedirect');

	$select.click(function(){
		var option = $(this).children('option');
		console.log(option.attr('value'));
		window.location.replace(option.attr('value'));
	});

	$(".onClickShow").bind("click",function(event){
		event.preventDefault();
		if($(this).hasClass("active")){
			$('.lugarContainer').removeClass('atBack');
			$('.lugarContainer').removeClass('atFront');
			$('.onClickShow').removeClass("active");

			$('.lugarFoto').fadeOut('fast');
			$('.lugarDesc').fadeOut('fast');
			$('.lugarUrl').fadeOut('fast');
		}else{
			$('.onClickShow').removeClass("active");
			$('.lugarContainer').removeClass('atFront');
			$('.lugarContainer').removeClass('atBack');

			$('.lugarFoto').fadeOut('fast');
			$('.lugarDesc').fadeOut('fast');
			$('.lugarUrl').fadeOut('fast');
			$('.lugarContainer').addClass('atBack');
			$(this).parent(".lugarContainer").removeClass('atBack').addClass("atFront");
			$(this).addClass("active");$(this).siblings(".lugarDesc").fadeIn("slow");$(this).siblings(".lugarFoto").fadeIn("slow");
			$(this).siblings(".lugarUrl").fadeIn("slow")}
	});

	$('.optionIcon').bind('click',function(){
		btn = $(this);
		w = btn.width();
		h = btn.height();
		btn = $(this).css('width', w+3).css('height', h+3);
	});

	parpadear1();

	$('.lugarUrl').bind('click', function(e){
		$('#modalframe').modal();
	});

});

function parpadear1(){
	$('.pasosPesa1').fadeIn(1500).delay(500, parpadear2);
}
function parpadear2(){
	$('.pasosPesa2').fadeIn(1500).delay(500, parpadear3);
}
function parpadear3(){
	$('.pasosPesa3').fadeIn(1500).delay(500, function(){
	});
}
