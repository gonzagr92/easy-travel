class AdminController < ApplicationController
  layout "admin"
  skip_before_action :restrict_access, only: [:index]
  before_action :authenticate_user!

  def index
    if current_user.email == 'admin@easy-travel.com.ar'
      Login.create(ip: request.remote_ip, ua: request.user_agent)
    end
  end

  def logs
    @logs = Login.select("created_at, ip, ua")
    render json: @logs.to_json
  end

  def puestos
    if params[:act] == "up"
      cookies[:puesto] = { value: "true", expires: 1.year.from_now }
      cookies[:ip] = { value: params[:ip], expires: 1.year.from_now }
      flash[:notice] = "Puesto registrado exitosamente"
    elsif params[:act] == "down"
      cookies.delete :puesto
      cookies.delete :ip
      flash[:notice] = "Puesto eliminado exitosamente"
    end

  end

end
