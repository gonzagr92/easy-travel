class IndexController < ApplicationController
  skip_before_action :restrict_access

  def index
  end

  def asistencia
  end

  def bodegas
    @zonas = Zona.joins(:bodegas).distinct;
  end

  def lugares
    @zona = Zona.find_by(:uid => params[:uid])

    case params[:form]
    when "restaurantes"
      @lugares = @zona.restaurantes
    when "bodegas"
      @lugares = @zona.bodegas
    when "excursiones"
      @lugares = @zona.excursions
    when "compras"
      @lugares = @zona.compras
    when "sitios"
      @lugares = @zona.sitios
    end


  end

  def checkin
  end

  def checkin_detail
    case params[:aerolinea]
    when "latam"
      @url = "https://www.latam.com/es_ar/apps/personas/checkinunified"
    when "aerolineas_argentinas"
      @url = "http://www.aerolineas.com.ar/es-ar/reservas_servicios/web_checkin"
    end
  end

  def services
  end

  def restaurantes
    @zonas = Zona.joins(:restaurantes).distinct;
  end

  def compras
    @zonas = Zona.joins(:compras).distinct;
  end

  def pesa_equipaje
    if cookies[:puesto] == "true"
      @wss = true
    end
  end

  def tour_services
  end

  def tour_info
  end

  def excursiones
    @zonas = Zona.joins(:excursions).distinct;
  end

  def sitios
    @zonas = Zona.joins(:sitios).distinct;
  end

  def sitio_single
    @sitio = Sitio.find_by(:uid => params[:uid])
  end

end
