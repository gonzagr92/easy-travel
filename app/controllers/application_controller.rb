class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :restrict_access

  def restrict_access
    redirect_to root_url unless user_signed_in?
  end

end
