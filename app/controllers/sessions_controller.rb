class SessionsController < Devise::SessionsController
  layout "admin"
  skip_before_action :restrict_access
end
